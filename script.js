let contador = 0;

function validas(c1, c2, c3){
    console.log("validas(" + c1.style + ")");
    let cor1 = c1.style.backgroudColor;
    let cor2 = c2.style.backgroudColor;
    let cor3 = c3.style.backgroudColor;
    // console.log("1(" + cor1 + ")(" + cor2 + ")(" + cor3 + ")");
    if (cor1 === "" || cor2 === "" || cor3 === ""){
        return false;
    }   
    if (cor1 === cor2 && cor2=== cor3){
        return true;
    }
    return false;
}

function verificarExistenciaVencedor(){
    let casas = document.querySelectorAll(".casa");

    console.log("1 " + casas[0]);
    return(validas(casas[0], casas[1], casas[2]) ||
           validas(casas[3], casas[4], casas[5]) ||
           validas(casas[6], casas[7], casas[8]) ||
           validas(casas[0], casas[3], casas[6]) ||
           validas(casas[1], casas[4], casas[7]) ||
           validas(casas[2], casas[5], casas[8]) ||
           validas(casas[0], casas[4], casas[8]) ||
           validas(casas[2], casas[4], casas[6])
        );
}


function encerrarJogo(vencedor){
    let casas = document.querySelectorAll(".casa");
    for (let i = 0; i<casas.length; i++){
        casas[i].onclick = null;
    }
    let divVencedor = document.querySelector("#vencedor");
    if(vencedor){
        if(contador%2){
            divVencedor.innerHTML = "Jogador Laranja Venceu";
            divVencedor.style = "color: #F65E01"
        }
        else{
            divVencedor.innerHTML = "Jogador Azul Venceu";
            divVencedor.style = "color: blue";
        }
    }
    else{
        divVencedor.innerHTML = "Empate";
    }
}

function jogar(){
    if(contador%2){
        this.style = "background-color: blue";
    } else{
        this.style = "background-color: #F65E01";
        }
    contador++;
    this.onclick = null;
    let vencedor = verificarExistenciaVencedor();
    // console.log(vencedor);

    if(vencedor || contador == 9){
        encerrarJogo(vencedor);
    }
}

let casas = document.querySelectorAll(".casa");

for (let i = 0; i<casas.length; i++){
    casas[i].onclick = jogar;
}